/***************************************************************************
** File Name: WordSearch.java
** Project: Enlighten Challenge - alphabet-soup
** Author: Blake Wishard
** Date: 4/2/20
**
** The WordSearch.java file consists of the WordSearch() class and the main driver. 
** The WordSearch() class contains a default constructor, overloaded contstructor, 
** a LoadFile() function to load the user-inputed .txt file, a SetFileName() function 
** to set the m_fileName, SetRowSize() and SetColumnSize() functions to set m_rowSize and 
** m_columnSize, SetNumCount() to set the m_numCount used to hold the number of words given
** in the .txt file to be found in the WordSearch, a FindWords() function that is used to 
** iterate through each row and column in the word search and check if the character at 
** that position is equivalent to the first letter of the keyword to be found, a SearchForWord()
** function that is passed the current keyword, row, and column from FindWords() and is used 
** to look in each of the 8 possible direction to see if the rest of the letters of that keyword
** are in that same consecutive direction, a SearchSouth, SearchEast, SouthWest, SearchNorth,
** SearchSouthEast, SearchSouthWest, SearchNorthEast, and SearchNorthWest function to look in each 
** of the 8 directions. The private member variables for the WordSearch() class consist of 
** m_rowSize and m_columnSize used to hold the size of the rows and columns of the wordsearch 
** given in the .txt file, m_numCounter used to hold the number of words given to find in the wordsearch,
** m_foundCounter used to hold the number of words found in the wordsearch, m_fileName uesd to 
** hold the user inputed file name, char grid[][] which is a 2D array used to create a grid to hold 
** the characters for the wordsearch, m_keyWords which is an ArrayList used to the given words to find 
** in the word search, m_foundWords which is an ArrayList used to hold the words that have been found in
** the wordsearch. 
****************************************************************************/
import java.util.*;
import java.util.ArrayList;
import java.io.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.lang.reflect.Array;
import java.util.Arrays;

public class WordSearch {

    //private member variables
    private int MAX_GRID_ROW_SIZE = 100;    //constant for the maximum row size of the grid
    private int MAX_GRID_COLUMN_SIZE = 100; //constant for the maximum column size of the grid
    private int m_rowSize;      //holds the number of rows in the grid
    private int m_columnSize;   //holds the number of columns in the grid 
    private int m_numCount;     //holds the number of words to be found in the WordSearch
    private int m_foundCounter; //holds the number of words that have been found in the WordSearch
    private String m_fileName;  //holds the user-inputed file name
    char grid[][] = new char[MAX_GRID_ROW_SIZE][MAX_GRID_COLUMN_SIZE]; //2D array that stores the characters in each row and column
    ArrayList<String> m_keyWords = new ArrayList<String>();    //arrayList that is used to hold the words to be found in the WordSearch
    ArrayList<String> m_foundWords = new ArrayList<String>(); //arrayLIst that is used to hold the words that have been found in the WordSearch


    //Default constructer for the WordSearch() class 
    public WordSearch() {
    
        //initializes the private member variables
        m_rowSize = 0;
        m_columnSize = 0;
        m_numCount = 0; 
        m_foundCounter = 0;
        m_fileName = " ";
    }

    //Overloaded constructor for the WordSearch() class 
    public WordSearch(int rowSize, int columnSize, int numCount) {

        //initializes the private member variables to their passed values 
        m_rowSize = rowSize;     
        m_columnSize = columnSize; 
        m_numCount = numCount; 
        m_fileName = " ";
    }

    //Sets the private member variable m_fileName
    public final void SetFileName(String fileName) {
    
        m_fileName = fileName; 
    }

    //Sets the private member variable m_rowSize
    public final void SetRowSize(int rowSize) {

        m_rowSize = rowSize;
    }

    //Sets the private member variable m_columnSize
    public final void SetColumnSize(int columnSize) {

        m_columnSize = columnSize;
    }

    //Sets the private member variable m_numCount
    public final void SetNumCount(int numCount) {

        m_numCount = numCount;  
    }

    //Loads the file. Uses passed file name to open the .txt file and 
    //format it correctly for use with the WordSearch
    public final void LoadFile(String fileName) throws IOException {

        SetFileName(fileName); //sets the m_filenName to the passed, user-inputed file name

        int rowSize = 0;        //initializes the row size to 0
        int columnSize = 0;     //initializes the column size to 0
        int numCount = 0;       //initializes the number of words to be found to 0

        //try catch for Exception
        try {
            
            BufferedReader reader = new BufferedReader(new FileReader(fileName)); //creates a BufferedReader to read text from the character input stream in the .txt file
            String fileLine = reader.readLine();  //creates a new string to hold each line of the .txt file 

            int xPos = fileLine.indexOf("x"); //an int to hold the 'x' char in between the row and column size

            rowSize = Integer.parseInt(fileLine.substring(0, xPos)); //the size of the rows in the wordsearch
            SetRowSize(rowSize); //sets m_rowSize

            columnSize = Integer.parseInt(fileLine.substring(xPos+1)); //the size of the columns in the wordsearch
            SetColumnSize(columnSize);  //sets m_columnSize

            fileLine = reader.readLine(); //reads the next line 
           
            int currRow = 0; //sets the current row to 0 
            
            //loop while the current row does not equal the max size of the rows in the word search (m_rowSize)
            while (currRow != m_rowSize) {
                
                fileLine = fileLine.replaceAll(" ", ""); //removes the spaces in between the chars
                
                //for loop used to iterate through each row and column in the file and insert each character in the text file 
                //into each of the grid[i][j] index positions. Additionally, displays each character to the screen.
                for (int c = 0; c < m_columnSize; c++) { 
                    grid[currRow][c] = fileLine.charAt(c);
                }
                currRow++;
                
                //if currRow is not at the last row, read the next line 
                if (currRow != m_rowSize) {
                fileLine = reader.readLine();
                }
            }
            
            //while there is still lines left in the .txt file, add them to the 
            //m_keyWords ArrayList
            while ((fileLine = reader.readLine()) != null) {
                m_keyWords.add(fileLine); //adds the string keyword to the ArrayList
            } 

            reader.close(); //closes the reader
            

            //prints the wordsearch to the screen 
            for (int i = 0; i < m_rowSize; i++) {
                for (int j = 0; j < m_columnSize; j++) {
  
                  System.out.print(grid[i][j] + " ");
                }
                System.out.println("");
              }

            //prints the keyword to find to the screen
            for (int i = 0; i < m_keyWords.size(); i++) {
                System.out.println(m_keyWords.get(i));
            }
            System.out.print("\n");
           
        }
        catch (Exception e) { //catches Exception
            
        }

        numCount = m_keyWords.size(); //used to hold the number of given words to be found in the WordSearch from the .txt file

        SetNumCount(numCount); //sets m_numCount, which is used to hold the number of words to be found
    }

        

    //Uses a nested for loop to iterate through the grid to check if the current position is the first letter
    //of the current keyword. Will continue looping until every word is found in the grid
    public final void FindWords() { 

        //Will continue to loop until every given keyword from the .txt file is found in the WordSearch
        while (m_foundCounter != m_numCount) {

            String keyWord = m_keyWords.get(m_foundCounter); //holds the given keyWord to be found.
                                                            //updates after each word is found
            //System.out.print("m_keyWords at 0 = ");
           // System.out.print(m_keyWords.get(m_foundCounter));

            //nested for loop used to iterate through each row and column in the grid to check if that current position
            //is potentially the first letter for the current keyword. If the first letter of the current keyword is found,
            //it will call SearchForWord() and pass it the current keyword, along with the current row and column position. 
            //if SearchForWord() returns true, this indicates the given keyword was found in the word search. Then, if the 
            //number of found words (m_foundCounter) is not equal to the number of words to be found (m_numCount), then the 
            //keyword is updated to the next word to be found. This will continue until every given word from the .txt file
            //is found in the WordSearch.                                   
            for (int j = 0; j < m_columnSize; j++) {
            
                for (int i = 0; i < m_rowSize; i++) {

                    //if the current grid postion is equal to the first character of the keyword, call SearchForWord to check 
                    //if the remaining letters of that word are located in any of the 8 possible positions, consecutively. 
                    if (grid[i][j] == keyWord.charAt(0)) {
                    
                        //if SearchForWord returns true, this indicates the keyword was found. Else, the loop will continue to the 
                        //next position in the grid.
                        if (SearchForWord(keyWord, i, j)) {
                        
                            // if the number of found words (m_foundCounter) is not equal to the number of words to be found (m_numCount), 
                            //then the keyword is updated to the next word to be found. If the number of found words equal the number of 
                            //words to be found, this indcates the progam is complete and it wil stop looping.
                            if (m_foundCounter != m_numCount) {
                            
                                keyWord = m_keyWords.get(m_foundCounter); //updates the keyword to be found 
                            }
                            else {
                                return;
                            }
                            break;
                        }  
                    }
                }
            }
        }
    }

    //Is called in FindWords(). If the current grid position in FindWords() is the potential first letter of a keyword, 
    //SearchForWord is called to check if the second letter of the keyword is found in any of the 8 possible directions from that position-
    //(down, right, left, up, down and to the right, down and to the left, up and to the left, and up and to the right). 
    //If the second letter of that keyword is found in any of those positions, the program will call the specific function 
    //for that direction, and check if the remaining letters of the keyword are found by ONLY continuing in that specific 
    //direction. 
    //If the second letter of the keyword is not found in any of those 8 positions, the program will return false- 
    //indicating that position is not valid for the first letter of the given keyword. 
    //If the second letter is found in one of those 8 positions, and the specific direction function is called, only for the 
    //program to realize that it is a false alarm and that position does not contain the valid first letter for that keyword, the
    //program will return false, and continue on and check in the remaining, un-checked, directions. 
    public boolean SearchForWord(String keyWord, int i, int j) {

        int charIndex = 0;  //initializes the character index to 0. Used to look for each of the character of the keyword 
        int letterCount = 0; //intializes the letter count to 0. Used to count the number of letters found for that keyword

        int row = 0;    //initializes the row to 0
        int column = 0; //initializes the column to 0
 
        boolean foundWord = false; //boolean used to return if the keyword is found or not
        
        //if the passed grid position is equal to the first letter of the keyword, updates the row, column, charIndex, and letterCount
        if (grid[i][j] == keyWord.charAt(0)) {
            row = i;       //sets the row equal to the passed row value 
            column = j;    //sets the column equal to the passed column value  
            charIndex++;   //updates the charIndex 
            letterCount++; //updates the letterCount     
            
            try {
                //Looks to see if the second letter of the keyword is found in the position directly south from it
                if (grid[row+1][column] == keyWord.charAt(charIndex)) {
        
                    //if the second letter is found in the position directly south, calls SearchSouth() to check if 
                    //the remaining letters of the word are found by continuing down the grid. If SearchSouth() returns
                    //true, the keyword is found and SearchFoWord returns true, to update and look for the next keyword.
                    //Else, program continues to check the remaining positions
                    if (SearchSouth(keyWord, row, column)) {
                        return foundWord;
                    }
                }
            } catch(ArrayIndexOutOfBoundsException e) { //catches exception

            }
         
            try { 
                //looks to see if the second letter of the keyword is found in the position directly east from it
                if (grid[row][column+1] == keyWord.charAt(charIndex)) {
            
                    //if the second letter is found in the position directly to the east, calls SearchEast() to check if 
                    //the remaining letters of the word are found by continuing to the right of the grid. If SearchEast() returns
                    //true, the keyword is found and SearchForWord returns true, to update and look for the next keyword.
                    //Else, program continues to check the remaining positions
                    if (SearchEast(keyWord, row, column)) {
                        return foundWord;
                    }
                }
            } catch (ArrayIndexOutOfBoundsException e) { //catches exception
            }
            
            try { 
                //looks to see if the second letter of the keyword is found in the position directly to the west of it
                if (grid[row][column-1] == keyWord.charAt(charIndex)) {

                    //if the second letter is found in the position directly to the west, calls SearchWest() to check if 
                    //the remaining letters of the word are found by continuing to the left of the grid. If SearchWest() returns
                    //true, the keyword is found and SearchForWord returns true, to update and look for the next keyword.
                    //Else, program continues to check the remaining positions
                    if (SearchWest(keyWord, row, column)) {
                        return foundWord;
                    } 
                }
            } catch (ArrayIndexOutOfBoundsException e) { //catches exception

            }

            try {
                //looks to see if the second letter of the keyword is found in the position directly north from it
                if (grid[row-1][column] == keyWord.charAt(charIndex)) {
            
                    //if the second letter is found in the position directly to the north, calls SearchNorth() to check if 
                    //the remaining letters of the word are found by continuing up the grid. If SearchNorth() returns
                    //true, the keyword is found and SearchForWord returns true, to update and look for the next keyword.
                    //Else, program continues to check the remaining positions
                    if(SearchNorth(keyWord, row, column)) {
                        return foundWord;
                    } 
                }
            } catch(ArrayIndexOutOfBoundsException e) { //catches exception

            }

        
            try {
                //looks to see if the second letter of the keyword is found in the position directly to the south east from it
                if (grid[row+1][column+1] == keyWord.charAt(charIndex)) {

                    //if the second letter is found in the position directly to the south east, calls SearchSouthEast() to check if 
                    //the remaining letters of the word are found by continuing down and to the right of the grid. If SearchSouthEast() 
                    //returns true, the keyword is found and SearchForWord returns true, to update and look for the next keyword.
                    //Else, program continues to check the remaining positions
                    if (SearchSouthEast(keyWord, row, column)) {
                        return foundWord;
                    }
                }
            } catch (ArrayIndexOutOfBoundsException e) { //catches exception
            
            }
        
            try {
                //looks to see if the second letter of the keyword is found in the position directly to the south west from it
                if (grid[row+1][column-1] == keyWord.charAt(charIndex)) {
            
                    //if the second letter is found in the position directly to the south west, calls SearchSouthWest() to check if 
                    //the remaining letters of the word are found by continuing down and to the left of the grid. If SearchSouthWest() 
                    //returns true, the keyword is found and SearchForWord returns true, to update and look for the next keyword.
                    //Else, program continues to check the remaining positions
                    if(SearchSouthWest(keyWord, row, column)) {
                        return foundWord;
                    }
                }
            } catch (ArrayIndexOutOfBoundsException e) { //catches exception

            }
            
            try {
                //looks to see if the second letter of the keyword is found in the position directly to the north east from it
                if (grid[row-1][column+1] == keyWord.charAt(charIndex)) {

                    //if the second letter is found in the position directly to the north east, calls SearchNorthEast() to check if 
                    //the remaining letters of the word are found by continuing up and to the right of the grid. If SearchNorthEast() 
                    //returns true, the keyword is found and SearchForWord returns true, to update and look for the next keyword.
                    //Else, program continues to check the remaining positions
                    if(SearchNorthEast(keyWord, row, column)) {
                        return foundWord;
                    } 
                }
            } catch (ArrayIndexOutOfBoundsException e) { //catches exception
 
            }
            
            try {
                //looks to see if the second letter of the keyword is found in the position directly to the north west from it
                if (grid[row-1][column-1] == keyWord.charAt(charIndex)) {
            
                    //if the second letter is found in the position directly to the north west, calls SearchNorthWest() to check if 
                    //the remaining letters of the word are found by continuing up and to the left of the grid. If SearchNorthWest() 
                    //returns true, the keyword is found and SearchForWord returns true, to update and look for the next keyword.
                    //Else, program continues to check the remaining positions
                    if(SearchNorthWest(keyWord, row, column)) {
                        return foundWord;
                    }
                }
            } catch (ArrayIndexOutOfBoundsException e) { //catches exception

            }
        }
        return foundWord;     
    }

    //Used to search in the south direction of the grid
    public boolean SearchSouth(String keyWord, int row, int column) {

        int keyWordSize = keyWord.length(); //used to hold the size of the keyword 
  
        int charIndex = 1;   //sets the character index to look for to 1
        int letterCount = 1; //sets the found letter count to 1

        int startingRow = row;        //sets the starting row to the passed row value
        int startingColumn = column;  //sets the starting column to the passed column value 

        int endingRow = 0;           //initializes the starting row to 0
        int endingColumn = 0;        //initializes the starting column to 0

        boolean foundWord = false;      //boolean used to indicate if a word was found or not

        try {
            //if the second letter of the keyword is found in the position directly south from it, updates row, letterCount, and charIndex
            if (grid[row+1][column] == keyWord.charAt(charIndex)) {
                row++;          
                letterCount++; 
                charIndex++;    
    
                //while loop that will run and look for letters to the south as long as the letterCount is not the same size as the keyword, 
                //and the rows and columns do not go out of bounds
                while ((letterCount != keyWordSize) &&  (row >= 0 && row < m_rowSize) && (column >= 0 && column < m_columnSize)) {
                
                    try {
                        //if the next letter of the keyword is found in the south position, update row, letterCount, and charIndex
                        if (grid[row+1][column] == keyWord.charAt(charIndex)) {
                            row++; 
                            letterCount++;
                            charIndex++;
                        }
                        //else, return false
                        else {
                            return foundWord;
                        }
            
                        //if the letterCount is the same as the keyword size, set the foundWord bool to true, set the ending row and column
                        //to the current row and column, add the keyword to the found word ArrayList (m_foundWords), print the found keyword, 
                        //increase the found keyword counter, and return true 
                        if (letterCount == keyWordSize) {
                            foundWord = true; //sets the bool to true  
                            endingRow = row;  //sets the ending row to the current row
                            endingColumn = column; //sets the ending column to the current column 
                            m_foundWords.add(m_keyWords.get(m_foundCounter)); //adds the found keyword to the found words ArrayList
                           
                            //prints the found keyWords and their starting and ending location
                            System.out.print(m_foundWords.get(m_foundCounter));
                            System.out.print(" " + startingRow + ":" + startingColumn + " " + endingRow + ":" + endingColumn + "\n");
                            
                            m_foundCounter++;  //increases the found word counter
                            return foundWord;  //returns true 
                        }
                    } catch (ArrayIndexOutOfBoundsException e) { //catches exception
                        return foundWord;
                    }
                }
            }
           
        } catch (ArrayIndexOutOfBoundsException e) { //catches exception
        
        }

        return foundWord; //else, returns false 
    }
     
    //Used to search in the east direction of the grid
    public boolean SearchEast(String keyWord, int row, int column) {

        int keyWordSize = keyWord.length(); //used to hold the size of the keyword 
    
        int charIndex = 1;   //sets the character index to look for to 1
        int letterCount = 1; //sets the found letter count to 1

        int startingRow = row;        //sets the starting row to the passed row value
        int startingColumn = column;  //sets the starting column to the passed column value 

        int endingRow = 0;           //initializes the starting row to 0
        int endingColumn = 0;        //initializes the starting column to 0

        boolean foundWord = false;      //boolean used to indicate if a word was found or not

        try { 
            //if the second letter of the keyword is found in the position directly east from it, updates row, letterCount, and charIndex
            if (grid[row][column+1] == keyWord.charAt(charIndex)) {
                column++;    
                letterCount++;  
                charIndex++;
    
                //while loop that will run and look for letters to the east as long as the letterCount is not the same size as the keyword, 
                //and the rows and columns do not go out of bounds
                while ((letterCount != keyWordSize) &&  (row >= 0 && row < m_rowSize) && (column >= 0 && column < m_columnSize)) {
                    
                    try { 
                        //if the next letter of the keyword is found in the east position, update column, letterCount, and charIndex
                        if (grid[row][column+1] == keyWord.charAt(charIndex)) {
                            column++; 

                            letterCount++;
                            charIndex++;
                        }
                        //else, return false
                        else {
                            return foundWord;
                        }
            
                        //if the letterCount is the same as the keyword size, set the foundWord bool to true, set the ending row and column
                        //to the current row and column, add the keyword to the found word ArrayLsit (m_foundWords), print the found keyword, 
                        //increase the found keyword counter, and return true
                        if (letterCount == keyWordSize) {
                            foundWord = true; //sets the bool to true  
                            endingRow = row;  //sets the ending row to the current row
                            endingColumn = column; //sets the ending column to the current column 
                            m_foundWords.add(m_keyWords.get(m_foundCounter)); //adds the found keyword to the found words ArrayList
                           
                            //prints the found keyWords and their starting and ending location
                            System.out.print(m_foundWords.get(m_foundCounter));
                            System.out.print(" " + startingRow + ":" + startingColumn + " " + endingRow + ":" + endingColumn + "\n");
                            
                            m_foundCounter++;  //increases the found word counter
                            return foundWord;  //returns true 
                        }  

                    } catch (ArrayIndexOutOfBoundsException e) { //catches exception

                    }
                    
                }
            }
    
        } catch (ArrayIndexOutOfBoundsException e) { //catches exception

        }
        return foundWord; //else, returns false 
    }

    //Used to search in the west direction of the grid
    public boolean SearchWest(String keyWord, int row, int column) {

        int keyWordSize = keyWord.length(); //used to hold the size of the keyword 
  
        int charIndex = 1;   //sets the character index to look for to 1
        int letterCount = 1; //sets the found letter count to 1

        int startingRow = row;        //sets the starting row to the passed row value
        int startingColumn = column;  //sets the starting column to the passed column value 

        int endingRow = 0;           //initializes the starting row to 0
        int endingColumn = 0;        //initializes the starting column to 0

        boolean foundWord = false;      //boolean used to indicate if a word was found or not

        try { 
            //if the second letter of the keyword is found in the position directly west from it, updates column, letterCount, and charIndex
            if (grid[row][column-1] == keyWord.charAt(charIndex)) {
                column--;
                letterCount++;
                charIndex++;
    
                //while loop that will run and look for letters to the west as long as the letterCount is not the same size as the keyword, 
                //and the rows and columns do not go out of bounds
                while ((letterCount != keyWordSize) &&  (row >= 0 && row < m_rowSize) && (column >= 0 && column < m_columnSize)) {
                
                    try {
                        //if the next letter of the keyword is found in the west position, updates column, letterCount, and charIndex
                        if (grid[row][column-1] == keyWord.charAt(charIndex)) {
                            column--;
                            letterCount++;
                            charIndex++;
                        }
                        //else, returns false
                        else {
                            return foundWord;
                        }
            
                        //if the letterCount is the same as the keyword size, set the foundWord bool to true, set the ending row and column
                        //to the current row and column, add the keyword to the found word ArrayList (m_foundWords), print the found keyword, 
                        //increase the found keyword counter, and return true
                        if (letterCount == keyWordSize) {
                            foundWord = true; //sets the bool to true  
                            endingRow = row;  //sets the ending row to the current row
                            endingColumn = column; //sets the ending column to the current column 
                            m_foundWords.add(m_keyWords.get(m_foundCounter)); //adds the found keyword to the found words ArrayList
                           
                            //prints the found keyWords and their starting and ending location
                            System.out.print(m_foundWords.get(m_foundCounter));
                            System.out.print(" " + startingRow + ":" + startingColumn + " " + endingRow + ":" + endingColumn + "\n");
                           
                            m_foundCounter++;  //increases the found word counter
                            return foundWord;  //returns true 
                        }   
                    } catch (ArrayIndexOutOfBoundsException e) { //catches exception

                    }
                    
                }
            }
           
        } catch (ArrayIndexOutOfBoundsException e) { //catches exception

        }
        return foundWord; //else, returns false
    }
    
    //Used to search in the north direction of the grid
    public boolean SearchNorth(String keyWord, int row, int column) {
 
        int keyWordSize = keyWord.length(); //used to hold the size of the keyword 
  
        int charIndex = 1;   //sets the character index to look for to 1
        int letterCount = 1; //sets the found letter count to 1

        int startingRow = row;        //sets the starting row to the passed row value
        int startingColumn = column;  //sets the starting column to the passed column value 

        int endingRow = 0;           //initializes the starting row to 0
        int endingColumn = 0;        //initializes the starting column to 0

        boolean foundWord = false;      //boolean used to indicate if a word was found or not

        try { 
            //if the second letter of the keyword is found in the position directly north from it, updates row, letterCount, and charIndex
            if (grid[row-1][column] == keyWord.charAt(charIndex)) {
                row--;
                letterCount++;
                charIndex++;
    
                //while loop that will run and look for letters to the north as long as the letterCount is not the same size as the keyword, 
                //and the rows and columns do not go out of bounds
                while ((letterCount != keyWordSize) &&  (row >= 0 && row < m_rowSize) && (column >= 0 && column < m_columnSize)) {
                
                    try {
                        //if the next letter of the keyword is found in the north position, update row, letterCount, and charIndex
                        if (grid[row-1][column] == keyWord.charAt(charIndex)) {
                            row--; 
                            letterCount++;
                            charIndex++;
                        }
                        //else, returns false 
                        else {
                           return foundWord;
                        }
            
                        //if the letterCount is the same as the keyword size, set the foundWord bool to true, set the ending row and column
                        //to the current row and column, add the keyword to the found word ArrayList (m_foundWords), print the found keyword, 
                        //increase the found keyword counter, and return true
                        if (letterCount == keyWordSize) {
                            foundWord = true; //sets the bool to true  
                            endingRow = row;  //sets the ending row to the current row
                            endingColumn = column; //sets the ending column to the current column 
                            m_foundWords.add(m_keyWords.get(m_foundCounter)); //adds the found keyword to the found words ArrayList
                           
                            //prints the found keyWords and their starting and ending location
                            System.out.print(m_foundWords.get(m_foundCounter));
                            System.out.print(" " + startingRow + ":" + startingColumn + " " + endingRow + ":" + endingColumn + "\n");
                            
                            m_foundCounter++;  //increases the found word counter
                            return foundWord;  //returns true 
                        }   
                    } catch (ArrayIndexOutOfBoundsException e) { //catches exception

                    }
                    
                }
            }     
           
        } catch (ArrayIndexOutOfBoundsException e) { //catches exception
            
        }
        return foundWord; //else, returns false 
    }

    //Used to search in the south east direction of the grid
    public boolean SearchSouthEast(String keyWord, int row, int column) {

        //System.out.println("searchSouthEast hit");
        int keyWordSize = keyWord.length(); //used to hold the size of the keyword
  
        int charIndex = 1;   //sets the character index to look for to 1
        int letterCount = 1; //sets the found letter count to 1

        int startingRow = row;        //sets the starting row to the passed row value
        int startingColumn = column;  //sets the starting column to the passed column value 

        int endingRow = 0;           //initializes the starting row to 0
        int endingColumn = 0;        //initializes the starting column to 0

        boolean foundWord = false;      //boolean used to indicate if a word was found or not

        try { 
            //if the second letter of the keyword is found in the position directly south east from it, updates row, colummn, letterCount, and charIndex
            if (grid[row+1][column+1] == keyWord.charAt(charIndex)) {
                row++;
                column++;
                letterCount++;
                charIndex++;
    
                //while loop that will run and look for letters to the north as long as the letterCount is not the same size as the keyword, 
                //and the rows and columns do not go out of bounds
                while ((letterCount != keyWordSize) &&  (row >= 0 && row < m_rowSize) && (column >= 0 && column < m_columnSize)) {
                    
                    try {
                        //if the next letter of the keyword is found in the south east position, update row,column, letterCount, and charIndex
                        if (grid[row+1][column+1] == keyWord.charAt(charIndex)) {
                            row++;
                            column++; 
                            letterCount++;
                            charIndex++;
                        }
                        //else, returns false 
                        else {
                            return foundWord;
                        }
            
                         //if the letterCount is the same as the keyword size, set the foundWord bool to true, set the ending row and column
                        //to the current row and column, add the keyword to the found word ArrayList (m_foundWords), print the found keyword, 
                        //increase the found keyword counter, and return true
                        if (letterCount == keyWordSize) {
                            foundWord = true; //sets the bool to true  
                            endingRow = row;  //sets the ending row to the current row
                            endingColumn = column; //sets the ending column to the current column 
                            m_foundWords.add(m_keyWords.get(m_foundCounter)); //adds the found keyword to the found words ArrayList
                           
                            //prints the found keyWords and their starting and ending location
                            System.out.print(m_foundWords.get(m_foundCounter));
                            System.out.print(" " + startingRow + ":" + startingColumn + " " + endingRow + ":" + endingColumn + "\n");
                            
                            m_foundCounter++;  //increases the found word counter
                            return foundWord;  //returns true  
                        }   
                    } catch (ArrayIndexOutOfBoundsException e) { //catches exception

                    }
                    
                }
                
            } 
            
        } catch (ArrayIndexOutOfBoundsException e) { //catches exception
         
        }
        return foundWord; //else, returns false 
    }
        
        //Used to search in the south west direction of the grid
        public boolean SearchSouthWest(String keyWord, int row, int column) {
    
            int keyWordSize = keyWord.length(); //used to hold the size of the keyword 
      
            int charIndex = 1;   //sets the character index to look for to 1
            int letterCount = 1; //sets the found letter count to 1
    
            int startingRow = row;        //sets the starting row to the passed row value
            int startingColumn = column;  //sets the starting column to the passed column value 
    
            int endingRow = 0;           //initializes the starting row to 0
            int endingColumn = 0;        //initializes the starting column to 0
    
            boolean foundWord = false;      //boolean used to indicate if a word was found or not
    
            try { 
                //if the second letter of the keyword is found in the position directly south west from it, updates row, column, letterCount, and charIndex
                if (grid[row+1][column-1] == keyWord.charAt(charIndex)) {
                    row++;
                    column--;
                    letterCount++;
                    charIndex++;
                
                    //while loop that will run and look for letters to the north as long as the letterCount is not the same size as the keyword, 
                    //and the rows and columns do not go out of bounds
                    while ((letterCount != keyWordSize) &&  (row >= 0 && row < m_rowSize) && (column >= 0 && column < m_columnSize)) {
                    
                        try {
                            //if the next letter of the keyword is found in the south west position, update row,column, letterCount, and charIndex
                            if (grid[row+1][column-1] == keyWord.charAt(charIndex)) {
                                row++;
                                column--;
                                letterCount++;
                                charIndex++;
                            }
                            //else, returns false 
                            else {
                                return foundWord;
                            }
                
                             //if the letterCount is the same as the keyword size, set the foundWord bool to true, set the ending row and column
                            //to the current row and column, add the keyword to the found word ArrayLIst (m_foundWords), print the found keyword, 
                            //increase the found keyword counter, and return true
                            if (letterCount == keyWordSize) {
                                foundWord = true; //sets the bool to true  
                                endingRow = row;  //sets the ending row to the current row
                                endingColumn = column; //sets the ending column to the current column 
                                m_foundWords.add(m_keyWords.get(m_foundCounter)); //adds the found keyword to the found words ArrayList
                               
                                //prints the found keyWords and their starting and ending location
                                System.out.print(m_foundWords.get(m_foundCounter));
                                System.out.print(" " + startingRow + ":" + startingColumn + " " + endingRow + ":" + endingColumn + "\n");
                                
                                m_foundCounter++;  //increases the found word counter
                                return foundWord;  //returns true 
                            }   
                        } catch (ArrayIndexOutOfBoundsException e) { //catches exception
                    }  
                }
            }
                
        } catch (ArrayIndexOutOfBoundsException e) { //catches exception

        }
        return foundWord; //else, returns false 
    }
            

    //Used to search in the north east direction of the grid
    public boolean SearchNorthEast(String keyWord, int row, int column) {

        int keyWordSize = keyWord.length(); //used to hold the size of the keyword 
      
        int charIndex = 1;   //sets the character index to look for to 1
        int letterCount = 1; //sets the found letter count to 1
    
        int startingRow = row;        //sets the starting row to the passed row value
        int startingColumn = column;  //sets the starting column to the passed column value 
    
        int endingRow = 0;           //initializes the starting row to 0
        int endingColumn = 0;        //initializes the starting column to 0
    
        boolean foundWord = false;      //boolean used to indicate if a word was found or not
        
        try { 
            //if the second letter of the keyword is found in the position directly north easet from it, updates row, column, letterCount, and charIndex
            if (grid[row-1][column+1] == keyWord.charAt(charIndex)) {
                row--;
                column++;
                letterCount++;
                charIndex++;
                
                //while loop that will run and look for letters to the north as long as the letterCount is not the same size as the keyword, 
                //and the rows and columns do not go out of bounds
                while ((letterCount != keyWordSize) &&  (row >= 0 && row < m_rowSize) && (column >= 0 && column < m_columnSize)) {
                    
                    try {
                        //if the next letter of the keyword is found in the north east position, update row,column, letterCount, and charIndex
                        if (grid[row-1][column+1] == keyWord.charAt(charIndex)) {
                            row--;
                            column++;
                            letterCount++;
                            charIndex++;
                        }
                        //else, returns false 
                        else {
                            return foundWord;
                        }
            
                        //if the letterCount is the same as the keyword size, set the foundWord bool to true, set the ending row and column
                        //to the current row and column, add the keyword to the found word ArrayList (m_foundWords), print the found keyword, 
                        //increase the found keyword counter, and return true
                        if (letterCount == keyWordSize) {
                            foundWord = true; //sets the bool to true  
                            endingRow = row;  //sets the ending row to the current row
                            endingColumn = column; //sets the ending column to the current column 
                            m_foundWords.add(m_keyWords.get(m_foundCounter)); //adds the found keyword to the found words ArrayList
                           
                            //prints the found keyWords and their starting and ending location
                            System.out.print(m_foundWords.get(m_foundCounter));
                            System.out.print(" " + startingRow + ":" + startingColumn + " " + endingRow + ":" + endingColumn + "\n");
                            
                            m_foundCounter++;  //increases the found word counter
                            return foundWord;  //returns true 
                        }   
                    } catch (ArrayIndexOutOfBoundsException e) { //catches exception

                    }
                }     
            }
        } catch (ArrayIndexOutOfBoundsException e) { //catches exception

        }
        return foundWord; //else, returns false 
    }
    
    //Used to search in the north west direction of the grid
    public boolean SearchNorthWest(String keyWord, int row, int column) {

        int keyWordSize = keyWord.length(); //used to hold the size of the keyword 
      
        int charIndex = 1;   //sets the character index to look for to 1
        int letterCount = 1; //sets the found letter count to 1
    
        int startingRow = row;        //sets the starting row to the passed row value
        int startingColumn = column;  //sets the starting column to the passed column value 
    
        int endingRow = 0;           //initializes the starting row to 0
        int endingColumn = 0;        //initializes the starting column to 0
    
        boolean foundWord = false;      //boolean used to indicate if a word was found or not
        
        try {
            //if the second letter of the keyword is found in the position directly north west from it, updates row, column, letterCount, and charIndex
            if (grid[row-1][column-1] == keyWord.charAt(charIndex)) {
                row--;
                column--;
                letterCount++;
                charIndex++;
        
                //while loop that will run and look for letters to the north as long as the letterCount is not the same size as the keyword, 
                //and the rows and columns do not go out of bounds
                while ((letterCount != keyWordSize) &&  (row >= 0 && row < m_rowSize) && (column >= 0 && column < m_columnSize)) {
                    
                    try {
                        //if the next letter of the keyword is found in the north west position, update row,column, letterCount, and charIndex
                        if (grid[row-1][column-1] == keyWord.charAt(charIndex)) {
                            row--;
                            column--;
                            letterCount++;
                            charIndex++;
                        }
                        else {
                            return foundWord;
                        }
            
                        //if the letterCount is the same as the keyword size, set the foundWord bool to true, set the ending row and column
                        //to the current row and column, add the keyword to the found word ArrayList (m_foundWords), print the found keyword, 
                        //increase the found keyword counter, and return true
                        if (letterCount == keyWordSize) {
                            foundWord = true; //sets the bool to true  
                            endingRow = row;  //sets the ending row to the current row
                            endingColumn = column; //sets the ending column to the current column 
                            m_foundWords.add(m_keyWords.get(m_foundCounter)); //adds the found keyword to the found words ArrayList
                           
                            //prints the found keyWords and their starting and ending location
                            System.out.print(m_foundWords.get(m_foundCounter));
                            System.out.print(" " + startingRow + ":" + startingColumn + " " + endingRow + ":" + endingColumn + "\n");
                            
                            m_foundCounter++;  //increases the found word counter
                            return foundWord;  //returns true 
                        }   
                    }catch (ArrayIndexOutOfBoundsException e) { //catches exception

                    }
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) { //catches exception
        
        }
        return foundWord; //else, returns fals  
    }
};


    












 









