/***************************************************************************
** File Name: Driver.java
** Project: Enlighten Challenge - alphabet-soup
** Author: Blake Wishard
** Date: 4/2/20
**
** File Description: Driver file for the WordSearch.java program
****************************************************************************/

import java.util.Scanner;
import java.io.*;

public class Driver {

    public static void main(String[] args) throws IOException {

        String fileName = " ";  //initializes the string to hold the file name 
        Scanner keyboard = new Scanner(System.in); //used to read user inputed data
        System.out.println("Enter the name of your file: "); //prompts the user to enter the name of the .txt file
        fileName = keyboard.nextLine(); //inserts the user-inputed file name into the fileName string
        System.out.print("\n");
        keyboard.close(); //closes the keyboard
        
        WordSearch W = new WordSearch();   //creates a WordSearch() object used to call WordSearch() funcitons 

        W.LoadFile(fileName); //calls LoadFile() passing it the fileName to open the .txt ile and insert the data in their specifc locatiosn 
        W.FindWords();        //calls FindWords() used to find all of the given words to find in the WordSearch
    }

}